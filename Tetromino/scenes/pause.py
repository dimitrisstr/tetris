from pygame import Surface, KEYUP, K_p

from hud import HUD
from scenes.base import Scene
from utilities import play_sound


class Pause(Scene):
    def __init__(self, game):
        super().__init__(game)
        play_sound('pause')
        screen_size = game.size()
        self.background = Surface(screen_size).convert_alpha()
        self.background.fill((0, 0, 0, 180))
        self.hud = HUD()
        self.hud.add_item(label='message',
                          position=(screen_size[0] / 2,
                                    screen_size[1] / 2 - 50),
                          size=100, color=(255, 255, 255),
                          value='PAUSE', center_x=True)
        self.rendered = False

    def event(self, event):
        # Unpause the game.
        if event.type == KEYUP and event.key == K_p:
            play_sound('pause')
            self.game.remove_scene()

    def render(self, surface):
        if not self.rendered:
            surface.blit(self.background, (0, 0))
            self.hud.render(surface)
            self.rendered = True
