from pygame import display, Surface

from config import ANIMATED_BACKGROUND
from grids.classic import ClassicGrid
from hud import HUD
from parallax import ParallaxBackground
from player import Player
from scenes.base import Scene
from utilities import ARROW_KEYS, LETTER_KEYS


class Multiplayer(Scene):
    SCREEN_WIDTH = 1280
    SCREEN_HEIGHT = 720
    BLOCK_SIZE = 30

    def __init__(self, game):
        super().__init__(game)
        self.resize()

        self.left_grid = ClassicGrid(self.BLOCK_SIZE)
        self.left_player = Player(self.left_grid, LETTER_KEYS)

        self.right_grid = ClassicGrid(self.BLOCK_SIZE)
        self.right_player = Player(self.right_grid, ARROW_KEYS)

        self.background = ParallaxBackground('background',
                                             (self.SCREEN_WIDTH,
                                              self.SCREEN_HEIGHT))

        panel_size = (self.SCREEN_WIDTH // 2, self.SCREEN_HEIGHT)
        self.left_panel = Surface(panel_size).convert_alpha()
        self.right_panel = Surface(panel_size).convert_alpha()

        self.game_over = False
        self.hud = HUD()
        self.hud.add_item(label='message',
                          position=(self.SCREEN_WIDTH // 2 + 70,
                                    self.SCREEN_HEIGHT // 2 - 50),
                          size=55,
                          color=(255, 255, 255),
                          value='',
                          visible=False,
                          center_x=True)

    def resize(self):
        display.set_mode((self.SCREEN_WIDTH, self.SCREEN_HEIGHT))

    def tick(self, dt):
        self.background.tick(dt)
        # The scene isn't removed immediately when the game over flag is set.
        # It is removed at the next tick because we want to render the hud
        # message before removing this scene. For this reason we check the if
        # the game is over at the start of the tick method.
        if self.left_player.is_game_over() or self.right_player.is_game_over():
            self.game.remove_scene()
            return

        self.left_player.tick(dt)
        self.right_player.tick(dt)

        self.right_player.insert_lines(self.left_player.get_lines_cleared())
        self.left_player.insert_lines(self.right_player.get_lines_cleared())

    def event(self, event):
        self.left_player.event(event)
        self.right_player.event(event)

    def render(self, surface):
        # Clear panels.
        self.left_panel.fill((0, 0, 0, 0))
        self.right_panel.fill((0, 0, 0, 0))

        self.left_player.render(self.left_panel)
        self.right_player.render(self.right_panel)

        self.background.render(surface, ANIMATED_BACKGROUND)
        surface.blit(self.left_panel, (0, 0))
        surface.blit(self.right_panel, (self.SCREEN_WIDTH // 2, 0))

        if self.left_player.is_game_over() or self.right_player.is_game_over():
            self._winner()
            self.hud.toggle_visibility('message')
            self.game.set_delay(2500)
        self.hud.render(surface)

    def _winner(self):
        left_over = self.left_player.is_game_over()
        right_over = self.right_player.is_game_over()
        if left_over and right_over:
            self.hud.update_value('message', 'TIE')
        elif left_over:
            self.hud.update_value('message', 'RIGHT PLAYER WINS')
        elif right_over:
            self.hud.update_value('message', 'LEFT PLAYER WINS')
