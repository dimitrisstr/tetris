from config import FPS
from game import Game


if __name__ == '__main__':
    game = Game(fps=FPS)

    while True:
        game.step()
