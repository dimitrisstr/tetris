from grids.grid import Grid
from utilities import load_sprite


class ClassicGrid(Grid):
    ROWS = 20
    COLUMNS = 10

    def __init__(self, block_size):
        dimensions = (block_size * self.COLUMNS, block_size * self.ROWS)
        background = load_sprite('background_grid', dimensions)
        super().__init__(self.ROWS, self.COLUMNS, block_size, background)
