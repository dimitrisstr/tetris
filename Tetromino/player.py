from pygame import key, KEYDOWN, KEYUP, Surface

from hud import HUD
from randomizer import BagRandomizer
from tetromino import Tetromino
from utilities import center_surface, play_sound


class Player:
    # Tetromino speed parameters
    INITIAL_MOVE_TIME = 1  # time in seconds
    MIN_MOVE_TIME = 0.1    # time in seconds

    # Difficulty parameters.
    DIFFICULTY_MODIFIER = 0.05
    # How many lines need to be cleared in order to increase difficulty.
    INC_DIF_LINES = 10

    TEXT_COLOR = (255, 255, 255)

    NEXT_TETROMINO_COORD = (64, 470)

    def __init__(self, grid, keys, columns=None):
        """
        :param (Grid) grid:
        :param (tuple) keys: (Left, Right, Rotate Clockwise, Rotate
        Counterclockwise, Drop, Speed Up)
        :param (tuple) columns: (start, end) start <= column_index < end
        """
        self.randomizer = BagRandomizer()
        self.block_size = grid.get_block_size()
        self.difficulty_level = 1
        self.drop_tetromino = False
        self.keys = keys
        self.lines_cleared = 0

        self.columns = columns
        self.grid = grid
        self.tetromino_move_time = Player.INITIAL_MOVE_TIME

        self.center_coord = self.grid.get_center_coord(columns)
        # current random tetromino
        self.current_tetromino = Tetromino(self.randomizer.get(),
                                           self.block_size,
                                           self.center_coord,
                                           self.tetromino_move_time)

        self.next_tetromino_coord = self.NEXT_TETROMINO_COORD
        # create next random tetromino
        self.next_tetromino = Tetromino(self.randomizer.get(),
                                        self.block_size,
                                        self.next_tetromino_coord,
                                        self.tetromino_move_time)

        self.grid_panel = Surface(self.grid.get_size()).convert_alpha()
        self.grid_panel.fill((0, 0, 0, 0))

        self.hud = HUD()
        self._init_hud()

    def get_lines_cleared(self):
        return self.lines_cleared

    def set_next_tetromino_position(self, position):
        self.next_tetromino_coord = position
        self.next_tetromino.set_coords(self.next_tetromino_coord)

    def is_game_over(self):
        return self.grid.is_game_over()

    def insert_lines(self, lines_number):
        """
        Insert gray block lines at the bottom of the grid.
        :param (int) lines_number: Number of lines to insert.
        """
        self.grid.insert_blocks(lines_number)

    def tick(self, dt):
        self.lines_cleared = 0
        # Drop tetromino.
        while True:
            self.current_tetromino.move_down(dt)
            overlap = self.grid.overlap(self.current_tetromino)
            if not self.drop_tetromino or overlap:
                self.drop_tetromino = False
                break

        # Tetromino overlaps a block or passed the bottom.
        if overlap:
            self.current_tetromino.move_up()
            self._reached_bottom()

    def event(self, event):
        """
        :param (pygame event) event:
        """
        def apply_move(move, undo_move):
            move(self.current_tetromino)
            if not self.grid.is_move_valid(self.current_tetromino, self.columns):
                undo_move(self.current_tetromino)

        keys_pressed = key.get_pressed()
        if event.type == KEYDOWN:
            if keys_pressed[self.keys[0]]:      # move left
                apply_move(Tetromino.move_left, Tetromino.move_right)
            elif keys_pressed[self.keys[1]]:    # move right
                apply_move(Tetromino.move_right, Tetromino.move_left)

            if keys_pressed[self.keys[2]]:      # rotate clockwise
                apply_move(Tetromino.rotate_cw, Tetromino.rotate_ccw)
            elif keys_pressed[self.keys[3]]:    # rotate counterclockwise
                apply_move(Tetromino.rotate_ccw, Tetromino.rotate_cw)

            if keys_pressed[self.keys[4]]:      # drop the tetromino instantly
                play_sound('drop')
                self.drop_tetromino = True
            elif keys_pressed[self.keys[5]]:    # increase tetromino speed
                self.current_tetromino.speed_up()
        elif event.type == KEYUP:
            if event.key == self.keys[5]:       # reset tetromino speed
                self.current_tetromino.reset_speed()

    def render(self, surface, hud=True, grid=True):
        """
        Render player hud and next tetromino at the surface. All the grid
        components are rendered at the grid panel, and then the grid panel
        is rendered at the surface.
        :param (Surface) surface:
        :param (bool) hud: If True, render the player hud.
        :param (bool) grid: If True, render the grid.
        """
        if hud:
            self.hud.render(surface)
        self.next_tetromino.render(surface)

        """ Render grid components. """
        self.grid_panel.fill((0, 0, 0, 0))  # clear surface
        if grid:
            self.grid.render(self.grid_panel)

        if not self.grid.is_game_over():
            self.current_tetromino.render(self.grid_panel)
            self.grid.render_assist_tetromino(self.grid_panel,
                                              self.current_tetromino)
        surface.blit(self.grid_panel,
                     center_surface(self.grid_panel,
                                    (0.6 * surface.get_width(),
                                     0.5 * surface.get_height())))

    def _init_hud(self):
        self.hud.add_item(label='score', position=(20, 50), size=38,
                          color=Player.TEXT_COLOR, value='SCORE: 0')
        self.hud.add_item(label='lines', position=(20, 100), size=38,
                          color=Player.TEXT_COLOR, value='LINES: 0')
        self.hud.add_item(label='level', position=(20, 150), size=38,
                          color=Player.TEXT_COLOR, value='LEVEL: 1')

        next_position = (self.NEXT_TETROMINO_COORD[0] - 10,
                         self.NEXT_TETROMINO_COORD[1] - 3 * self.block_size)
        self.hud.add_item(label='next', position=next_position, size=38,
                          color=Player.TEXT_COLOR, value='NEXT')

    def _update_hud(self):
        self.hud.update_value('score', f'SCORE: {self.grid.get_score()}')
        self.hud.update_value('lines', f'LINES: {self.grid.get_lines()}')
        self.hud.update_value('level', f'LEVEL: {self.difficulty_level}')

    def _increase_difficulty(self):
        self.difficulty_level += 1
        self.tetromino_move_time -= self.DIFFICULTY_MODIFIER
        self.tetromino_move_time = max(self.tetromino_move_time,
                                       self.MIN_MOVE_TIME)

    def _reached_bottom(self):
        self.lines_cleared = self.grid.get_lines()
        self.grid.insert_tetromino(self.current_tetromino)
        self.lines_cleared = self.grid.get_lines() - self.lines_cleared

        # Increase difficulty level every time INC_DIF_LINES lines are cleared.
        if self.grid.get_lines() // self.INC_DIF_LINES + 1\
                != self.difficulty_level:
            self._increase_difficulty()

        # Swap next and current tetromino and create a new tetromino.
        self.current_tetromino = self.next_tetromino
        self.current_tetromino.set_coords(self.center_coord)
        self.current_tetromino.set_speed(self.tetromino_move_time)
        self.next_tetromino = Tetromino(self.randomizer.get(),
                                        self.block_size,
                                        self.next_tetromino_coord,
                                        self.tetromino_move_time)
        self._update_hud()
