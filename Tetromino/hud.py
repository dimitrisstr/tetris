from os import path
from pygame import font

from utilities import write


class HUD:
    def __init__(self):
        self.font_path = path.join('assets', 'fonts', 'SadanaSquare.ttf')
        self.labels = {}
        self.fonts = {}

    def add_item(self, label, position, size, color, value='',
                 visible=True, center_x=False):
        """
        :param (int | str) label: This is the key of the hud item. It is used
        in other methods to refer to an item and change its properties.
        :param (tuple | list) position: (x, y)
        :param (int) size: Font size.
        :param (tuple) color: (r, g, b)
        :param (str) value: The value of the hud item.
        :param (bool) visible: Make this item visible or invisible.
        :param (bool) center_x: Center horizontally.
        """
        if size not in self.fonts:
            self.fonts[size] = font.Font(self.font_path, size)

        self.labels[label] = {
            'position': tuple(position),
            'font': self.fonts[size],
            'color': color,
            'text': value,
            'visible': visible,
            'center_x': center_x
        }

    def remove_item(self, label):
        self.labels.pop(label)

    def get_value(self, label):
        return self.labels[label]['text']

    def update_value(self, label, value):
        self.labels[label]['text'] = value

    def set_color(self, label, color):
        self.labels[label]['color'] = color

    def toggle_visibility(self, label):
        self.labels[label]['visible'] = not self.labels[label]['visible']

    def render(self, surface):
        for label in self.labels:
            element = self.labels[label]
            if element['visible']:
                text = write(element['font'],
                             element['text'],
                             element['color'])

                position = element['position']
                if element['center_x']:
                    position = self._center_x(text, position)

                surface.blit(text, (position[0], position[1]))

    @staticmethod
    def _center_x(text, position):
        x = position[0] - text.get_width() / 2
        return x, position[1]
